BEGIN FOR cur_rec IN(SELECT object_name, object_type FROM user_objects WHERE object_type IN ('TABLE', 'VIEW', 'PACKAGE', 'PROCEDURE', 'FUNCTION', 'SEQUENCE', 'SYNONYM', 'PACKAGE BODY')) LOOP BEGIN IF cur_rec.object_type = 'TABLE' THEN EXECUTE IMMEDIATE 'DROP ' || cur_rec.object_type || ' "' || cur_rec.object_name || '" CASCADE CONSTRAINTS'; ELSE EXECUTE IMMEDIATE 'DROP ' || cur_rec.object_type || ' "' || cur_rec.object_name || '"'; END IF; EXCEPTION WHEN OTHERS THEN DBMS_OUTPUT.put_line ( 'FAILED: DROP ' || cur_rec.object_type || ' "' || cur_rec.object_name || '"' ); END; END LOOP; END;
CREATE TABLE "Usuario" (
	"ID" NUMBER(9) NOT NULL,
	"Username" VARCHAR(15) NOT NULL UNIQUE,
	"Password" VARCHAR(15) NOT NULL,
	"Correo" VARCHAR(50) NULL UNIQUE,
	"Token" VARCHAR(32) NULL,
	"FechaRegistro" DATE DEFAULT SYSDATE NOT NULL,
	CONSTRAINT USUARIO_PK PRIMARY KEY ("ID")
);

CREATE TABLE "Rol" (
	"ID" NUMBER(9) NOT NULL,
    "Nombre" VARCHAR(25) NOT NULL,
    CONSTRAINT ROL_PK PRIMARY KEY ("ID")
);

CREATE TABLE "UsuarioRol" (
    "Usuario_ID" NUMBER(9) NOT NULL,
    "Rol_ID" NUMBER(9) NOT NULL,
    CONSTRAINT USUARIOROL_PK PRIMARY KEY ("Usuario_ID", "Rol_ID"),
    CONSTRAINT USUARIO_ROL_FK FOREIGN KEY ("Usuario_ID") REFERENCES "Usuario"("ID") ON DELETE CASCADE,
    CONSTRAINT ROL_USUARIO_FK FOREIGN KEY ("Rol_ID") REFERENCES "Rol"("ID") ON DELETE CASCADE   
);

CREATE TABLE "Region" (
    "ID" NUMBER(9) NOT NULL,
    "Nombre" VARCHAR(50) NOT NULL,
    CONSTRAINT REGION_PK PRIMARY KEY ("ID")
);

CREATE TABLE "Comuna" (
    "ID" NUMBER(9) NOT NULL,
    "Nombre" VARCHAR(50) NOT NULL,
    "Region_ID" NUMBER(9) NOT NULL,
    CONSTRAINT COMUNA_PK PRIMARY KEY ("ID"),
    CONSTRAINT REGION_COMUNA_FK FOREIGN KEY ("Region_ID") REFERENCES "Region"("ID") ON DELETE CASCADE
);

CREATE TABLE "Paciente" (
	"ID" NUMBER(9) NOT NULL,
	"Rut" VARCHAR(12) NOT NULL UNIQUE,
	"Nombres" VARCHAR(100) NOT NULL,
	"ApellidoPaterno" VARCHAR(25) NOT NULL,
	"ApellidoMaterno" VARCHAR(25) NOT NULL,
	"FechaNacimiento" DATE NOT NULL,
	"Direccion" VARCHAR(50) NOT NULL,
	"Telefono" VARCHAR(15) NOT NULL,
	"Comuna_ID" NUMBER(9) NOT NULL,
	"Region_ID" NUMBER(9) NOT NULL,
    CONSTRAINT PACIENTE_PK PRIMARY KEY ("ID"),
	CONSTRAINT USUARIO_PACIENTE_FK FOREIGN KEY ("ID") REFERENCES "Usuario"("ID") ON DELETE CASCADE,
	CONSTRAINT COMUNA_PACIENTE_FK FOREIGN KEY ("Comuna_ID") REFERENCES "Comuna"("ID"),
	CONSTRAINT REGION_PACIENTE_FK FOREIGN KEY ("Region_ID") REFERENCES "Region"("ID")
);

CREATE TABLE "TipoDocumento" (
	"ID" NUMBER(9) NOT NULL,
	"Nombre" VARCHAR(25) NOT NULL,
	CONSTRAINT TIPODOCUMENTO_PK PRIMARY KEY ("ID")
);

CREATE TABLE "Documento" (
	"ID" NUMBER(9) NOT NULL,
	"Nombre" VARCHAR(50) NOT NULL,
	"RutaArchivo" VARCHAR(100) NOT NULL,
	"Fecha" DATE DEFAULT SYSDATE NOT NULL,
	"TipoDocumento_ID" NUMBER(9) NOT NULL,
	CONSTRAINT DOCUMENTO_PK PRIMARY KEY ("ID"),
	CONSTRAINT TIPO_DOCUMENTO_FK FOREIGN KEY ("TipoDocumento_ID") REFERENCES "TipoDocumento"("ID")
);

CREATE TABLE "DocumentoPaciente" (
	"Documento_ID" NUMBER(9) NOT NULL,
	"Paciente_ID" NUMBER(9) NOT NULL,
	CONSTRAINT DOCUMENTOPACIENTE_PK PRIMARY KEY ("Documento_ID", "Paciente_ID"),
	CONSTRAINT DOCUMENTO_DOCPAC_FK FOREIGN KEY ("Documento_ID") REFERENCES "Documento"("ID") ON DELETE CASCADE,
	CONSTRAINT PACIENTE_DOCPAC_FK FOREIGN KEY ("Paciente_ID") REFERENCES "Paciente"("ID") ON DELETE CASCADE
);

CREATE TABLE "Especialidad" (
    "ID" NUMBER(9) NOT NULL,
    "Nombre" VARCHAR(50) NOT NULL,
    CONSTRAINT ESPECIALIDAD_PK PRIMARY KEY ("ID")
);

CREATE TABLE "Empleado" (
    "ID" NUMBER(9) NOT NULL,
    "Rut" VARCHAR(12) NOT NULL UNIQUE,
    "Nombres" VARCHAR(100) NOT NULL,
    "ApellidoPaterno" VARCHAR(25) NOT NULL,
    "ApellidoMaterno" VARCHAR(25) NOT NULL,
    "Telefono" VARCHAR(15) NOT NULL,
    CONSTRAINT EMPLEADO_PK PRIMARY KEY ("ID"), 
    CONSTRAINT USUARIO_EMPLEADO_FK FOREIGN KEY ("ID") REFERENCES "Usuario"("ID") ON DELETE CASCADE
);

CREATE TABLE "EspecialidadEmpleado" (
	"Empleado_ID" NUMBER(9) NOT NULL,
	"Especialidad_ID" NUMBER(9) NOT NULL,
	CONSTRAINT ESPECIALIDADEMPLEADO_PK PRIMARY KEY ("Empleado_ID", "Especialidad_ID"),
	CONSTRAINT EMPLEADO_ESPECIALIDAD_FK FOREIGN KEY ("Empleado_ID") REFERENCES "Empleado"("ID") ON DELETE CASCADE,
	CONSTRAINT ESPECIALIDAD_ESPECIALIDAD_FK FOREIGN KEY ("Especialidad_ID") REFERENCES "Especialidad"("ID") ON DELETE CASCADE
);


CREATE TABLE "Rubro" ( 
	"ID" NUMBER(9) NOT NULL,
	"Nombre" VARCHAR(25) NOT NULL,
	CONSTRAINT RUBRO_PK PRIMARY KEY ("ID")
);

CREATE TABLE "Proveedor" (
	"ID" NUMBER(9) NOT NULL,
	"Nombre" VARCHAR(50) NOT NULL,
	"Rut" VARCHAR(12) NOT NULL UNIQUE,
	"Telefono" VARCHAR(15) NOT NULL,
	"Rubro_ID" NUMBER(9) NOT NULL,
	CONSTRAINT PROVEEDOR_ID PRIMARY KEY ("ID"),
	CONSTRAINT RUBRO_PROVEEDOR_FK FOREIGN KEY ("Rubro_ID") REFERENCES "Rubro"("ID"),
	CONSTRAINT USUARIO_PROVEEDOR_FK FOREIGN KEY ("ID") REFERENCES "Usuario"("ID") ON DELETE CASCADE
);


CREATE TABLE "CategoriaProducto" (
	"ID" NUMBER(9) NOT NULL,
	"Nombre" VARCHAR(25) NOT NULL,
	CONSTRAINT CATEGORIAPRODUCTO_PK PRIMARY KEY ("ID")
);

CREATE TABLE "TipoProducto" (
	"ID" NUMBER(9) NOT NULL,
	"Nombre" VARCHAR(25) NOT NULL,
	"Categoria_ID" NUMBER(9) NOT NULL,
	CONSTRAINT TIPOPRODUCTO_PK PRIMARY KEY ("ID"),
	CONSTRAINT CATEGORIA_TIPO_FK FOREIGN KEY ("Categoria_ID") REFERENCES "CategoriaProducto"("ID") ON DELETE CASCADE
);

CREATE TABLE "Producto" (
	"ID" VARCHAR(17) NOT NULL,
	"Descripcion" VARCHAR(100) NOT NULL,
	"PrecioCompra" NUMBER(10) NOT NULL,
	"PrecioVenta" NUMBER(10) NOT NULL,
	"FechaVencimiento" DATE NOT NULL,
	"Categoria_ID" NUMBER(9) NOT NULL,
	"Proveedor_ID" NUMBER(9) NOT NULL,
	"Tipo_ID" NUMBER(9) NOT NULL,
	CONSTRAINT PRODUCTO_PK PRIMARY KEY ("ID"),
	CONSTRAINT CATEGORIA_PRODUCTO_FK FOREIGN KEY ("Categoria_ID") REFERENCES "CategoriaProducto"("ID") ON DELETE CASCADE,
	CONSTRAINT PROVEEDOR_PRODUCTO_FK FOREIGN KEY ("Proveedor_ID") REFERENCES "Proveedor"("ID") ON DELETE CASCADE,
	CONSTRAINT TIPO_PRODUCTO_FK FOREIGN KEY ("Tipo_ID") REFERENCES "TipoProducto"("ID") ON DELETE CASCADE
);

CREATE TABLE "Stock" (
	"Producto_ID" VARCHAR(17) NOT NULL,
	"Cantidad" NUMBER(9) NOT NULL,
	"Critico" NUMBER(9) NOT NULL,
	CONSTRAINT STOCK_PK PRIMARY KEY ("Producto_ID"),
	CONSTRAINT PRODUCTO_STOCK_FK FOREIGN KEY ("Producto_ID") REFERENCES "Producto"("ID") ON DELETE CASCADE
);

CREATE TABLE "Orden" (
	"ID" NUMBER(9) NOT NULL,
	"Empleado_ID" NUMBER(9) NOT NULL,
	"Proveedor_ID" NUMBER(9) NOT NULL,
	"TipoProducto_ID" NUMBER(9) NOT NULL,
	"Cantidad" NUMBER(9) NOT NULL,
	"FechaEmision" DATE DEFAULT SYSDATE NOT NULL,
	"Estado" NUMBER(2) DEFAULT 0 NOT NULL,
	CONSTRAINT ORDEN_PK PRIMARY KEY ("ID"),
	CONSTRAINT TIPOPRODUCTO_ORDEN_FK FOREIGN KEY ("TipoProducto_ID") REFERENCES "TipoProducto"("ID") ON DELETE CASCADE,
	CONSTRAINT EMPLEADO_ORDEN_FK FOREIGN KEY ("Empleado_ID") REFERENCES "Empleado"("ID") ON DELETE CASCADE,
	CONSTRAINT PROVEEDOR_ORDEN_FK FOREIGN KEY ("Proveedor_ID") REFERENCES "Proveedor"("ID") ON DELETE CASCADE
);

CREATE TABLE "RecepcionOrden" (
	"Orden_ID" NUMBER(9) NOT NULL,
	"Empleado_ID" NUMBER(9) NOT NULL,
	"FechaRecepcion" DATE DEFAULT SYSDATE NOT NULL,
	"Estado" NUMBER(1) NOT NULL,
	"Comentario" VARCHAR(100) NULL,
	CONSTRAINT RECEPCION_PK PRIMARY KEY ("Orden_ID"),
	CONSTRAINT ORDEN_RECEPCION_FK FOREIGN KEY ("Orden_ID") REFERENCES "Orden"("ID") ON DELETE CASCADE,
	CONSTRAINT EMPLEADO_RECEPCION_FK FOREIGN KEY ("Empleado_ID") REFERENCES "Empleado"("ID") ON DELETE CASCADE
);

CREATE TABLE "Servicio" (
	"ID" NUMBER(9) NOT NULL,
	"Nombre" VARCHAR(50) NOT NULL,
	"Descripcion" VARCHAR(100) NOT NULL,
	"Costo" NUMBER(9) NOT NULL,
	CONSTRAINT SERVICIO_PK PRIMARY KEY ("ID")
);

CREATE TABLE "Procedimiento" (
	"ID" NUMBER(9) NOT NULL,
	"Servicio_ID" NUMBER(9) NOT NULL,
	"Comentario" VARCHAR(200) NOT NULL,
	CONSTRAINT PROCEDIMIENTO_PK PRIMARY KEY ("ID"),
	CONSTRAINT SERVICIO_PROCEDIMIENTO_FK FOREIGN KEY ("Servicio_ID") REFERENCES "Servicio"("ID") ON DELETE CASCADE
);

CREATE TABLE "ProcedimientoDetalle" (
	"Procedimiento_ID" NUMBER (9) NOT NULL,
	"Producto_ID" VARCHAR(17) NOT NULL,
	"Cantidad" NUMBER(9) NOT NULL,
	CONSTRAINT PROCEDETALLE_PK PRIMARY KEY ("Procedimiento_ID", "Producto_ID"),
	CONSTRAINT PROCEDIMIENTO_DETALLE_FK FOREIGN KEY ("Procedimiento_ID") REFERENCES "Procedimiento"("ID") ON DELETE CASCADE,
	CONSTRAINT PRODUCTO_DETALLE_FK FOREIGN KEY ("Producto_ID") REFERENCES "Producto"("ID") ON DELETE CASCADE
);


CREATE TABLE "ReservaAtencion" (
	"ID" NUMBER(9) NOT NULL,
	"FechaReserva" DATE DEFAULT SYSDATE NOT NULL,
	"FechaAtencion" DATE NOT NULL,
	"Empleado_ID" NUMBER(9) NOT NULL,
	"Paciente_ID" NUMBER(9) NOT NULL,
	CONSTRAINT RESERVA_PK PRIMARY KEY ("ID"),
	CONSTRAINT EMPLEADO_RESERVA_FK FOREIGN KEY ("Empleado_ID") REFERENCES "Empleado"("ID") ON DELETE CASCADE,
	CONSTRAINT PACIENTE_RESERVA_FK FOREIGN KEY ("Paciente_ID") REFERENCES "Paciente"("ID") ON DELETE CASCADE
);


CREATE TABLE "Atencion" (
	"ID" NUMBER(9) NOT NULL,
	"Empleado_ID" NUMBER(9) NOT NULL,
	"Paciente_ID" NUMBER(9) NOT NULL,
	"FechaAtencion" DATE DEFAULT SYSDATE NOT NULL,
	"Observacion" VARCHAR(200) NOT NULL,
	CONSTRAINT ATENCION_PK PRIMARY KEY ("ID"),
	CONSTRAINT EMPLEADO_ATENCION_FK FOREIGN KEY ("Empleado_ID") REFERENCES "Empleado"("ID") ON DELETE CASCADE,
	CONSTRAINT PACIENTE_ATENCION_FK FOREIGN KEY ("Paciente_ID") REFERENCES "Paciente"("ID") ON DELETE CASCADE
);

CREATE TABLE "AtencionDetalle" (
	"Atencion_ID" NUMBER(9) NOT NULL,
	"Procedimiento_ID" NUMBER(9) NOT NULL,
	CONSTRAINT ATENCIONDETALLE_PK PRIMARY KEY ("Atencion_ID", "Procedimiento_ID"),
	CONSTRAINT ATENC_ATENCIONDETALLE_FK FOREIGN KEY ("Atencion_ID") REFERENCES "Atencion"("ID") ON DELETE CASCADE,
	CONSTRAINT PROCE_ATENCIONDETALLE_FK FOREIGN KEY ("Procedimiento_ID") REFERENCES "Procedimiento"("ID") ON DELETE CASCADE
);

CREATE TABLE "Boleta" (
	"ID" NUMBER(9) NOT NULL,
	"Empleado_ID" NUMBER(9) NOT NULL,
	"Atencion_ID" NUMBER(9) NOT NULL,
	"FechaEmision" DATE DEFAULT SYSDATE NOT NULL,
	"Documento_ID" NUMBER(9) NOT NULL,
	CONSTRAINT BOLETA_PK PRIMARY KEY ("ID"),
	CONSTRAINT EMPLEADO_BOLETA_FK FOREIGN KEY ("Empleado_ID") REFERENCES "Empleado"("ID") ON DELETE CASCADE,
	CONSTRAINT ATENCION_BOLETA_FK FOREIGN KEY ("Atencion_ID") REFERENCES "Atencion"("ID") ON DELETE CASCADE,
	CONSTRAINT DOCUMENTO_BOLETA_FK FOREIGN KEY ("Documento_ID") REFERENCES "Documento"("ID") ON DELETE CASCADE
);

BEGIN
	EXECUTE IMMEDIATE 'DROP SEQUENCE "ID_Usuario"';
	EXECUTE IMMEDIATE 'DROP SEQUENCE "ID_TipoDocumento"';
	EXECUTE IMMEDIATE 'DROP SEQUENCE "ID_Documento"';
	EXECUTE IMMEDIATE 'DROP SEQUENCE "ID_Especialidad"';
	EXECUTE IMMEDIATE 'DROP SEQUENCE "ID_Rubro"';
	EXECUTE IMMEDIATE 'DROP SEQUENCE "ID_CategoriaProducto"';
	EXECUTE IMMEDIATE 'DROP SEQUENCE "ID_TipoProducto"';
	EXECUTE IMMEDIATE 'DROP SEQUENCE "ID_Orden"';
	EXECUTE IMMEDIATE 'DROP SEQUENCE "ID_Servicio"';
	EXECUTE IMMEDIATE 'DROP SEQUENCE "ID_Procedimiento"';
	EXECUTE IMMEDIATE 'DROP SEQUENCE "ID_ReservaAtencion"';
	EXECUTE IMMEDIATE 'DROP SEQUENCE "ID_Atencion"';
	EXECUTE IMMEDIATE 'DROP SEQUENCE "ID_Boleta"';
EXCEPTION
	WHEN OTHERS THEN
         DBMS_OUTPUT.PUT_LINE('');
END;

CREATE SEQUENCE "ID_Usuario";
CREATE OR REPLACE TRIGGER "TRIGGER_Usuario"
	BEFORE INSERT OR UPDATE ON "Usuario" FOR EACH ROW
BEGIN
    IF INSERTING THEN
        :NEW."ID" := "ID_Usuario".nextval;
        :NEW."FechaRegistro" := SYSDATE;
    END IF;
    :NEW."Username" := LOWER(:NEW."Username");
    :NEW."Correo" := LOWER(:NEW."Correo");
    :NEW."Token" := DBMS_OBFUSCATION_TOOLKIT.md5 (input => UTL_RAW.cast_to_raw(:NEW."Username"||:NEW."Password"));
END;
/
CREATE OR REPLACE TRIGGER "TRIGGER_Paciente"
	BEFORE INSERT OR UPDATE ON "Paciente" FOR EACH ROW
BEGIN
	:NEW."Nombres" := UPPER(:NEW."Nombres");
	:NEW."ApellidoPaterno" := UPPER(:NEW."ApellidoPaterno");
	:NEW."ApellidoMaterno" := UPPER(:NEW."ApellidoMaterno");
END; 
/
CREATE SEQUENCE "ID_TipoDocumento";
CREATE OR REPLACE TRIGGER "TRIGGER_TipoDocumento"
	BEFORE INSERT OR UPDATE ON "TipoDocumento" FOR EACH ROW
BEGIN
	IF INSERTING THEN
		:NEW."ID" := "ID_TipoDocumento".nextval;
	END IF;
END;
/
CREATE SEQUENCE "ID_Documento";
CREATE OR REPLACE TRIGGER "TRIGGER_Documento"
	BEFORE INSERT OR UPDATE ON "Documento" FOR EACH ROW
BEGIN
	IF INSERTING THEN
		:NEW."ID" := "ID_Documento".nextval;
	END IF;
END;

CREATE SEQUENCE "ID_Especialidad";
CREATE OR REPLACE TRIGGER "TRIGGER_Especialidad"
	BEFORE INSERT OR UPDATE ON "Especialidad" FOR EACH ROW
BEGIN
	IF INSERTING THEN
		:NEW."ID" := "ID_Especialidad".nextval;
	END IF;
END;

CREATE OR REPLACE TRIGGER "TRIGGER_Empleado"
	BEFORE INSERT OR UPDATE ON "Empleado" FOR EACH ROW
BEGIN
	:NEW."Nombres" := UPPER(:NEW."Nombres");
	:NEW."ApellidoPaterno" := UPPER(:NEW."ApellidoPaterno");
	:NEW."ApellidoMaterno" := UPPER(:NEW."ApellidoMaterno");
END; 

CREATE SEQUENCE "ID_Rubro";
CREATE OR REPLACE TRIGGER "TRIGGER_Rubro"
	BEFORE INSERT OR UPDATE ON "Rubro" FOR EACH ROW
BEGIN
	IF INSERTING THEN
		:NEW."ID" := "ID_Rubro".nextval;
	END IF;
END;

CREATE OR REPLACE TRIGGER "TRIGGER_Proveedor"
	BEFORE INSERT OR UPDATE ON "Proveedor" FOR EACH ROW
BEGIN
	:NEW."Nombre" := UPPER(:NEW."Nombre");
END; 

CREATE SEQUENCE "ID_CategoriaProducto";
CREATE OR REPLACE TRIGGER "TRIGGER_CategoriaProducto"
	BEFORE INSERT OR UPDATE ON "CategoriaProducto" FOR EACH ROW
BEGIN
	IF INSERTING THEN
		:NEW."ID" := "ID_CategoriaProducto".nextval;
	END IF;
END;

CREATE SEQUENCE "ID_TipoProducto";
CREATE OR REPLACE TRIGGER "TRIGGER_TipoProducto"
	BEFORE INSERT OR UPDATE ON "TipoProducto" FOR EACH ROW
BEGIN
	IF INSERTING THEN
		:NEW."ID" := "ID_TipoProducto".nextval;
	END IF;
END;

CREATE OR REPLACE TRIGGER "TRIGGER_Producto"
	BEFORE INSERT ON "Producto" FOR EACH ROW
BEGIN
	:NEW."ID" := LPAD(TO_CHAR(:NEW."Proveedor_ID"),3,'0') || LPAD(TO_CHAR(:NEW."Categoria_ID"),3,'0') || to_char(:NEW."FechaVencimiento", 'yyyymmdd') ||LPAD(TO_CHAR(:NEW."Tipo_ID"),3,'0');
END;

CREATE SEQUENCE "ID_Orden";
CREATE OR REPLACE TRIGGER "TRIGGER_Orden"
	BEFORE INSERT OR UPDATE ON "Orden" FOR EACH ROW
BEGIN
	IF INSERTING THEN
		:NEW."ID" := "ID_Orden".nextval;
	END IF;
END;

CREATE SEQUENCE "ID_Servicio";
CREATE OR REPLACE TRIGGER "TRIGGER_Servicio"
	BEFORE INSERT OR UPDATE ON "Servicio" FOR EACH ROW
BEGIN
	IF INSERTING THEN
		:NEW."ID" := "ID_Servicio".nextval;
	END IF;
END;
/
CREATE SEQUENCE "ID_Procedimiento";
CREATE OR REPLACE TRIGGER "TRIGGER_Procedimiento"
	BEFORE INSERT OR UPDATE ON "Procedimiento" FOR EACH ROW
BEGIN
	IF INSERTING THEN
		:NEW."ID" := "ID_Procedimiento".nextval;
	END IF;
END;
/
CREATE SEQUENCE "ID_ReservaAtencion";
CREATE OR REPLACE TRIGGER "TRIGGER_ReservaAtencion"
	BEFORE INSERT OR UPDATE ON "ReservaAtencion" FOR EACH ROW
BEGIN
	IF INSERTING THEN
		:NEW."ID" := "ID_ReservaAtencion".nextval;
	END IF;
END;

CREATE SEQUENCE "ID_Atencion";
CREATE OR REPLACE TRIGGER "TRIGGER_Atencion"
	BEFORE INSERT OR UPDATE ON "Atencion" FOR EACH ROW
BEGIN
	IF INSERTING THEN
		:NEW."ID" := "ID_Atencion".nextval;
	END IF;
END;

CREATE SEQUENCE "ID_Boleta";
CREATE OR REPLACE TRIGGER "TRIGGER_Boleta"
	BEFORE INSERT OR UPDATE ON "Boleta" FOR EACH ROW
BEGIN
	IF INSERTING THEN
		:NEW."ID" := "ID_Boleta".nextval;
	END IF;
END;

/*
CREATE SEQUENCE "ID_";
CREATE OR REPLACE TRIGGER "TRIGGER_"
	BEFORE INSERT OR UPDATE ON "" FOR EACH ROW
BEGIN
	IF INSERTING THEN
		:NEW."ID" := "ID_".nextval;
	END IF;
END;
*/

INSERT ALL
	INTO "Usuario" VALUES (null, 'paciente', '123', 'paciente@paciente.com', null, null)
	INTO "Usuario" VALUES (null, 'empleado', '123', 'empleado@empleado.com', null, null)
	INTO "Usuario" VALUES (null, 'proveedor', '123', null, null, null)
	INTO "Usuario" VALUES (null, 'administrador', '123', 'admin@admin.com', null, null)
	INTO "Rol" VALUES (1, 'Administrador')
	INTO "Rol" VALUES (2, 'Empleado')
	INTO "Rol" VALUES (3, 'Paciente')
	INTO "Rol" VALUES (4, 'Proveedor')
	INTO "UsuarioRol" VALUES (1, 3)
	INTO "UsuarioRol" VALUES (2, 2)
	INTO "UsuarioRol" VALUES (3, 4)
	INTO "UsuarioRol" VALUES (4, 1)
SELECT dual.DUMMY FROM dual;

Insert into PORTAFOLIO."Region" (ID,"Nombre") values ('1','Arica y Parinacota');
Insert into PORTAFOLIO."Region" (ID,"Nombre") values ('2','Tarapacá');
Insert into PORTAFOLIO."Region" (ID,"Nombre") values ('3','Antofagasta');
Insert into PORTAFOLIO."Region" (ID,"Nombre") values ('4','Atacama');
Insert into PORTAFOLIO."Region" (ID,"Nombre") values ('5','Coquimbo');
Insert into PORTAFOLIO."Region" (ID,"Nombre") values ('6','Valparaíso');
Insert into PORTAFOLIO."Region" (ID,"Nombre") values ('7','Metropolitana de Santiago');
Insert into PORTAFOLIO."Region" (ID,"Nombre") values ('8','Libertador Gral. Bernardo O’Higgins');
Insert into PORTAFOLIO."Region" (ID,"Nombre") values ('9','Maule');
Insert into PORTAFOLIO."Region" (ID,"Nombre") values ('10','Biobío');
Insert into PORTAFOLIO."Region" (ID,"Nombre") values ('11','Araucanía');
Insert into PORTAFOLIO."Region" (ID,"Nombre") values ('12','Los Ríos');
Insert into PORTAFOLIO."Region" (ID,"Nombre") values ('13','Los Lagos');
Insert into PORTAFOLIO."Region" (ID,"Nombre") values ('14','Aysén del Gral. Carlos Ibáñez del Campo');
Insert into PORTAFOLIO."Region" (ID,"Nombre") values ('15','Magallanes y de la Antártica Chilena');


INSERT ALL
INTO "Comuna" VALUES (1, 'Arica', 1)
INTO "Comuna" VALUES (2, 'Camarones', 1)
INTO "Comuna" VALUES (3, 'General Lagos', 1)
INTO "Comuna" VALUES (4, 'Putre', 1)
INTO "Comuna" VALUES (5, 'Alto Hospicio', 2)
INTO "Comuna" VALUES (6, 'Camiña', 2)
INTO "Comuna" VALUES (7, 'Colchane', 2)
INTO "Comuna" VALUES (8, 'Huara', 2)
INTO "Comuna" VALUES (9, 'Iquique', 2)
INTO "Comuna" VALUES (10, 'Pica', 2)
INTO "Comuna" VALUES (11, 'Pozo Almonte', 2)
INTO "Comuna" VALUES (12, 'Antofagasta', 3)
INTO "Comuna" VALUES (13, 'Calama', 3)
INTO "Comuna" VALUES (14, 'María Elena', 3)
INTO "Comuna" VALUES (15, 'Mejillones', 3)
INTO "Comuna" VALUES (16, 'Ollagüe', 3)
INTO "Comuna" VALUES (17, 'San Pedro de Atacama', 3)
INTO "Comuna" VALUES (18, 'Sierra Gorda', 3)
INTO "Comuna" VALUES (19, 'Taltal', 3)
INTO "Comuna" VALUES (20, 'Tocopilla', 3)
INTO "Comuna" VALUES (21, 'Alto del Carmen', 4)
INTO "Comuna" VALUES (22, 'Caldera', 4)
INTO "Comuna" VALUES (23, 'Chañaral', 4)
INTO "Comuna" VALUES (24, 'Copiapó', 4)
INTO "Comuna" VALUES (25, 'Diego de Almagro', 4)
INTO "Comuna" VALUES (26, 'Freirina', 4)
INTO "Comuna" VALUES (27, 'Huasco', 4)
INTO "Comuna" VALUES (28, 'Tierra Amarilla', 4)
INTO "Comuna" VALUES (29, 'Vallenar', 4)
INTO "Comuna" VALUES (30, 'Andacollo', 5)
INTO "Comuna" VALUES (31, 'Canela', 5)
INTO "Comuna" VALUES (32, 'Combarbalá', 5)
INTO "Comuna" VALUES (33, 'Coquimbo', 5)
INTO "Comuna" VALUES (34, 'Illapel', 5)
INTO "Comuna" VALUES (35, 'La Higuera', 5)
INTO "Comuna" VALUES (36, 'La Serena', 5)
INTO "Comuna" VALUES (37, 'Los Vilos', 5)
INTO "Comuna" VALUES (38, 'Monte Patria', 5)
INTO "Comuna" VALUES (39, 'Ovalle', 5)
INTO "Comuna" VALUES (40, 'Paiguano', 5)
INTO "Comuna" VALUES (41, 'Punitaqui', 5)
INTO "Comuna" VALUES (42, 'Río Hurtado', 5)
INTO "Comuna" VALUES (43, 'Salamanca', 5)
INTO "Comuna" VALUES (44, 'Vicuña', 5)
INTO "Comuna" VALUES (45, 'Algarrobo', 6)
INTO "Comuna" VALUES (46, 'Cabildo', 6)
INTO "Comuna" VALUES (47, 'Calera', 6)
INTO "Comuna" VALUES (48, 'Calle Larga', 6)
INTO "Comuna" VALUES (49, 'Cartagena', 6)
INTO "Comuna" VALUES (50, 'Casablanca', 6)
INTO "Comuna" VALUES (51, 'Catemu', 6)
INTO "Comuna" VALUES (52, 'Concón', 6)
INTO "Comuna" VALUES (53, 'El Quisco', 6)
INTO "Comuna" VALUES (54, 'El Tabo', 6)
INTO "Comuna" VALUES (55, 'Hijuelas', 6)
INTO "Comuna" VALUES (56, 'Isla de Pascua', 6)
INTO "Comuna" VALUES (57, 'Juan Fernández', 6)
INTO "Comuna" VALUES (58, 'La Cruz', 6)
INTO "Comuna" VALUES (59, 'La Ligua', 6)
INTO "Comuna" VALUES (60, 'Limache', 6)
INTO "Comuna" VALUES (61, 'Llaillay', 6)
INTO "Comuna" VALUES (62, 'Los Andes', 6)
INTO "Comuna" VALUES (63, 'Nogales', 6)
INTO "Comuna" VALUES (64, 'Olmué', 6)
INTO "Comuna" VALUES (65, 'Panquehue', 6)
INTO "Comuna" VALUES (66, 'Papudo', 6)
INTO "Comuna" VALUES (67, 'Petorca', 6)
INTO "Comuna" VALUES (68, 'Puchuncaví', 6)
INTO "Comuna" VALUES (69, 'Putaendo', 6)
INTO "Comuna" VALUES (70, 'Quillota', 6)
INTO "Comuna" VALUES (71, 'Quilpué', 6)
INTO "Comuna" VALUES (72, 'Quintero', 6)
INTO "Comuna" VALUES (73, 'Rinconada', 6)
INTO "Comuna" VALUES (74, 'San Antonio', 6)
INTO "Comuna" VALUES (75, 'San Esteban', 6)
INTO "Comuna" VALUES (76, 'San Felipe', 6)
INTO "Comuna" VALUES (77, 'Santa María', 6)
INTO "Comuna" VALUES (78, 'Santo Domingo', 6)
INTO "Comuna" VALUES (79, 'Valparaíso', 6)
INTO "Comuna" VALUES (80, 'Villa Alemana', 6)
INTO "Comuna" VALUES (81, 'Viña del Mar', 6)
INTO "Comuna" VALUES (82, 'Zapallar', 6)
INTO "Comuna" VALUES (83, 'Alhué', 7)
INTO "Comuna" VALUES (84, 'Buin', 7)
INTO "Comuna" VALUES (85, 'Calera de Tango', 7)
INTO "Comuna" VALUES (86, 'Cerrillos', 7)
INTO "Comuna" VALUES (87, 'Cerro Navia', 7)
INTO "Comuna" VALUES (88, 'Colina', 7)
INTO "Comuna" VALUES (89, 'Conchalí', 7)
INTO "Comuna" VALUES (90, 'Curacaví', 7)
INTO "Comuna" VALUES (91, 'El Bosque', 7)
INTO "Comuna" VALUES (92, 'El Monte', 7)
INTO "Comuna" VALUES (93, 'Estación Central', 7)
INTO "Comuna" VALUES (94, 'Huechuraba', 7)
INTO "Comuna" VALUES (95, 'Independencia', 7)
INTO "Comuna" VALUES (96, 'Isla de Maipo', 7)
INTO "Comuna" VALUES (97, 'La Cisterna', 7)
INTO "Comuna" VALUES (98, 'La Florida', 7)
INTO "Comuna" VALUES (99, 'La Granja', 7)
INTO "Comuna" VALUES (100, 'La Pintana', 7)
INTO "Comuna" VALUES (101, 'La Reina', 7)
INTO "Comuna" VALUES (102, 'Lampa', 7)
INTO "Comuna" VALUES (103, 'Las Condes', 7)
INTO "Comuna" VALUES (104, 'Lo Barnechea', 7)
INTO "Comuna" VALUES (105, 'Lo Espejo', 7)
INTO "Comuna" VALUES (106, 'Lo Prado', 7)
INTO "Comuna" VALUES (107, 'Macul', 7)
INTO "Comuna" VALUES (108, 'Maipú', 7)
INTO "Comuna" VALUES (109, 'María Pinto', 7)
INTO "Comuna" VALUES (110, 'Melipilla', 7)
INTO "Comuna" VALUES (111, 'Ñuñoa', 7)
INTO "Comuna" VALUES (112, 'Padre Hurtado', 7)
INTO "Comuna" VALUES (113, 'Paine', 7)
INTO "Comuna" VALUES (114, 'Pedro Aguirre Cerda', 7)
INTO "Comuna" VALUES (115, 'Peñaflor', 7)
INTO "Comuna" VALUES (116, 'Peñalolén', 7)
INTO "Comuna" VALUES (117, 'Pirque', 7)
INTO "Comuna" VALUES (118, 'Providencia', 7)
INTO "Comuna" VALUES (119, 'Pudahuel', 7)
INTO "Comuna" VALUES (120, 'Puente Alto', 7)
INTO "Comuna" VALUES (121, 'Quilicura', 7)
INTO "Comuna" VALUES (122, 'Quinta Normal', 7)
INTO "Comuna" VALUES (123, 'Recoleta', 7)
INTO "Comuna" VALUES (124, 'Renca', 7)
INTO "Comuna" VALUES (125, 'San Bernardo', 7)
INTO "Comuna" VALUES (126, 'San Joaquín', 7)
INTO "Comuna" VALUES (127, 'San José de Maipo', 7)
INTO "Comuna" VALUES (128, 'San Miguel', 7)
INTO "Comuna" VALUES (129, 'San Pedro', 7)
INTO "Comuna" VALUES (130, 'San Ramón', 7)
INTO "Comuna" VALUES (131, 'Santiago Centro', 7)
INTO "Comuna" VALUES (132, 'Talagante', 7)
INTO "Comuna" VALUES (133, 'Tiltil', 7)
INTO "Comuna" VALUES (134, 'Vitacura', 7)
INTO "Comuna" VALUES (135, 'Chépica', 8)
INTO "Comuna" VALUES (136, 'Chimbarongo', 8)
INTO "Comuna" VALUES (137, 'Codegua', 8)
INTO "Comuna" VALUES (138, 'Coinco', 8)
INTO "Comuna" VALUES (139, 'Coltauco', 8)
INTO "Comuna" VALUES (140, 'Doñihue', 8)
INTO "Comuna" VALUES (141, 'Graneros', 8)
INTO "Comuna" VALUES (142, 'La Estrella', 8)
INTO "Comuna" VALUES (143, 'Las Cabras', 8)
INTO "Comuna" VALUES (144, 'Litueche', 8)
INTO "Comuna" VALUES (145, 'Lolol', 8)
INTO "Comuna" VALUES (146, 'Machalí', 8)
INTO "Comuna" VALUES (147, 'Malloa', 8)
INTO "Comuna" VALUES (148, 'Marchihue', 8)
INTO "Comuna" VALUES (149, 'Mostazal', 8)
INTO "Comuna" VALUES (150, 'Nancagua', 8)
INTO "Comuna" VALUES (151, 'Navidad', 8)
INTO "Comuna" VALUES (152, 'Olivar', 8)
INTO "Comuna" VALUES (153, 'Palmilla', 8)
INTO "Comuna" VALUES (154, 'Paredones', 8)
INTO "Comuna" VALUES (155, 'Peralillo', 8)
INTO "Comuna" VALUES (156, 'Peumo', 8)
INTO "Comuna" VALUES (157, 'Pichidegua', 8)
INTO "Comuna" VALUES (158, 'Pichilemu', 8)
INTO "Comuna" VALUES (159, 'Placilla', 8)
INTO "Comuna" VALUES (160, 'Pumanque', 8)
INTO "Comuna" VALUES (161, 'Quinta de Tilcoco', 8)
INTO "Comuna" VALUES (162, 'Rancagua', 8)
INTO "Comuna" VALUES (163, 'Rengo', 8)
INTO "Comuna" VALUES (164, 'Requínoa', 8)
INTO "Comuna" VALUES (165, 'San Fernando', 8)
INTO "Comuna" VALUES (166, 'San Vicente', 8)
INTO "Comuna" VALUES (167, 'Santa Cruz', 8)
INTO "Comuna" VALUES (168, 'Cauquenes', 9)
INTO "Comuna" VALUES (169, 'Chanco', 9)
INTO "Comuna" VALUES (170, 'Colbún', 9)
INTO "Comuna" VALUES (171, 'Constitución', 9)
INTO "Comuna" VALUES (172, 'Curepto', 9)
INTO "Comuna" VALUES (173, 'Curicó', 9)
INTO "Comuna" VALUES (174, 'Empedrado', 9)
INTO "Comuna" VALUES (175, 'Hualañé', 9)
INTO "Comuna" VALUES (176, 'Licantén', 9)
INTO "Comuna" VALUES (177, 'Linares', 9)
INTO "Comuna" VALUES (178, 'Longaví', 9)
INTO "Comuna" VALUES (179, 'Maule', 9)
INTO "Comuna" VALUES (180, 'Molina', 9)
INTO "Comuna" VALUES (181, 'Parral', 9)
INTO "Comuna" VALUES (182, 'Pelarco', 9)
INTO "Comuna" VALUES (183, 'Pelluhue', 9)
INTO "Comuna" VALUES (184, 'Pencahue', 9)
INTO "Comuna" VALUES (185, 'Rauco', 9)
INTO "Comuna" VALUES (186, 'Retiro', 9)
INTO "Comuna" VALUES (187, 'Río Claro', 9)
INTO "Comuna" VALUES (188, 'Romeral', 9)
INTO "Comuna" VALUES (189, 'Sagrada Familia', 9)
INTO "Comuna" VALUES (190, 'San Clemente', 9)
INTO "Comuna" VALUES (191, 'San Javier', 9)
INTO "Comuna" VALUES (192, 'San Rafael', 9)
INTO "Comuna" VALUES (193, 'Talca', 9)
INTO "Comuna" VALUES (194, 'Teno', 9)
INTO "Comuna" VALUES (195, 'Vichuquén', 9)
INTO "Comuna" VALUES (196, 'Villa Alegre', 9)
INTO "Comuna" VALUES (197, 'Yerbas Buenas', 9)
INTO "Comuna" VALUES (198, 'Alto Biobío', 10)
INTO "Comuna" VALUES (199, 'Antuco', 10)
INTO "Comuna" VALUES (200, 'Arauco', 10)
INTO "Comuna" VALUES (201, 'Bulnes', 10)
INTO "Comuna" VALUES (202, 'Cabrero', 10)
INTO "Comuna" VALUES (203, 'Cañete', 10)
INTO "Comuna" VALUES (204, 'Chiguayante', 10)
INTO "Comuna" VALUES (205, 'Chillán', 10)
INTO "Comuna" VALUES (206, 'Chillán Viejo', 10)
INTO "Comuna" VALUES (207, 'Cobquecura', 10)
INTO "Comuna" VALUES (208, 'Coelemu', 10)
INTO "Comuna" VALUES (209, 'Coihueco', 10)
INTO "Comuna" VALUES (210, 'Concepción', 10)
INTO "Comuna" VALUES (211, 'Contulmo', 10)
INTO "Comuna" VALUES (212, 'Coronel', 10)
INTO "Comuna" VALUES (213, 'Curanilahue', 10)
INTO "Comuna" VALUES (214, 'El Carmen', 10)
INTO "Comuna" VALUES (215, 'Florida', 10)
INTO "Comuna" VALUES (216, 'Hualpén', 10)
INTO "Comuna" VALUES (217, 'Hualqui', 10)
INTO "Comuna" VALUES (218, 'Laja', 10)
INTO "Comuna" VALUES (219, 'Lebu', 10)
INTO "Comuna" VALUES (220, 'Los Álamos', 10)
INTO "Comuna" VALUES (221, 'Los Ángeles', 10)
INTO "Comuna" VALUES (222, 'Lota', 10)
INTO "Comuna" VALUES (223, 'Mulchén', 10)
INTO "Comuna" VALUES (224, 'Nacimiento', 10)
INTO "Comuna" VALUES (225, 'Negrete', 10)
INTO "Comuna" VALUES (226, 'Ninhue', 10)
INTO "Comuna" VALUES (227, 'Ñiquén', 10)
INTO "Comuna" VALUES (228, 'Pemuco', 10)
INTO "Comuna" VALUES (229, 'Penco', 10)
INTO "Comuna" VALUES (230, 'Pinto', 10)
INTO "Comuna" VALUES (231, 'Portezuelo', 10)
INTO "Comuna" VALUES (232, 'Quilaco', 10)
INTO "Comuna" VALUES (233, 'Quilleco', 10)
INTO "Comuna" VALUES (234, 'Quillón', 10)
INTO "Comuna" VALUES (235, 'Quirihue', 10)
INTO "Comuna" VALUES (236, 'Ránquil', 10)
INTO "Comuna" VALUES (237, 'San Carlos', 10)
INTO "Comuna" VALUES (238, 'San Fabián', 10)
INTO "Comuna" VALUES (239, 'San Ignacio', 10)
INTO "Comuna" VALUES (240, 'San Nicolás', 10)
INTO "Comuna" VALUES (241, 'San Pedro de la Paz', 10)
INTO "Comuna" VALUES (242, 'San Rosendo', 10)
INTO "Comuna" VALUES (243, 'Santa Bárbara', 10)
INTO "Comuna" VALUES (244, 'Santa Juana', 10)
INTO "Comuna" VALUES (245, 'Talcahuano', 10)
INTO "Comuna" VALUES (246, 'Tirúa', 10)
INTO "Comuna" VALUES (247, 'Tomé', 10)
INTO "Comuna" VALUES (248, 'Treguaco', 10)
INTO "Comuna" VALUES (249, 'Tucapel', 10)
INTO "Comuna" VALUES (250, 'Yumbel', 10)
INTO "Comuna" VALUES (251, 'Yungay', 10)
INTO "Comuna" VALUES (252, 'Angol', 11)
INTO "Comuna" VALUES (253, 'Carahue', 11)
INTO "Comuna" VALUES (254, 'Cholchol', 11)
INTO "Comuna" VALUES (255, 'Collipulli', 11)
INTO "Comuna" VALUES (256, 'Cunco', 11)
INTO "Comuna" VALUES (257, 'Curacautín', 11)
INTO "Comuna" VALUES (258, 'Curarrehue', 11)
INTO "Comuna" VALUES (259, 'Ercilla', 11)
INTO "Comuna" VALUES (260, 'Freire', 11)
INTO "Comuna" VALUES (261, 'Galvarino', 11)
INTO "Comuna" VALUES (262, 'Gorbea', 11)
INTO "Comuna" VALUES (263, 'Lautaro', 11)
INTO "Comuna" VALUES (264, 'Loncoche', 11)
INTO "Comuna" VALUES (265, 'Lonquimay', 11)
INTO "Comuna" VALUES (266, 'Los Sauces', 11)
INTO "Comuna" VALUES (267, 'Lumaco', 11)
INTO "Comuna" VALUES (268, 'Melipeuco', 11)
INTO "Comuna" VALUES (269, 'Nueva Imperial', 11)
INTO "Comuna" VALUES (270, 'Padre las Casas', 11)
INTO "Comuna" VALUES (271, 'Perquenco', 11)
INTO "Comuna" VALUES (272, 'Pitrufquén', 11)
INTO "Comuna" VALUES (273, 'Pucón', 11)
INTO "Comuna" VALUES (274, 'Purén', 11)
INTO "Comuna" VALUES (275, 'Renaico', 11)
INTO "Comuna" VALUES (276, 'Saavedra', 11)
INTO "Comuna" VALUES (277, 'Temuco', 11)
INTO "Comuna" VALUES (278, 'Teodoro Schmidt', 11)
INTO "Comuna" VALUES (279, 'Toltén', 11)
INTO "Comuna" VALUES (280, 'Traiguén', 11)
INTO "Comuna" VALUES (281, 'Victoria', 11)
INTO "Comuna" VALUES (282, 'Vilcún', 11)
INTO "Comuna" VALUES (283, 'Villarrica', 11)
INTO "Comuna" VALUES (284, 'Corral', 12)
INTO "Comuna" VALUES (285, 'Futrono', 12)
INTO "Comuna" VALUES (286, 'La Unión', 12)
INTO "Comuna" VALUES (287, 'Lago Ranco', 12)
INTO "Comuna" VALUES (288, 'Lanco', 12)
INTO "Comuna" VALUES (289, 'Los Lagos', 12)
INTO "Comuna" VALUES (290, 'Máfil', 12)
INTO "Comuna" VALUES (291, 'Mariquina', 12)
INTO "Comuna" VALUES (292, 'Paillaco', 12)
INTO "Comuna" VALUES (293, 'Panguipulli', 12)
INTO "Comuna" VALUES (294, 'Río Bueno', 12)
INTO "Comuna" VALUES (295, 'Valdivia', 12)
INTO "Comuna" VALUES (296, 'Ancud', 13)
INTO "Comuna" VALUES (297, 'Calbuco', 13)
INTO "Comuna" VALUES (298, 'Castro', 13)
INTO "Comuna" VALUES (299, 'Chaitén', 13)
INTO "Comuna" VALUES (300, 'Chonchi', 13)
INTO "Comuna" VALUES (301, 'Cochamó', 13)
INTO "Comuna" VALUES (302, 'Curaco de Vélez', 13)
INTO "Comuna" VALUES (303, 'Dalcahue', 13)
INTO "Comuna" VALUES (304, 'Fresia', 13)
INTO "Comuna" VALUES (305, 'Frutillar', 13)
INTO "Comuna" VALUES (306, 'Futaleufú', 13)
INTO "Comuna" VALUES (307, 'Hualaihué', 13)
INTO "Comuna" VALUES (308, 'Llanquihue', 13)
INTO "Comuna" VALUES (309, 'Los Muermos', 13)
INTO "Comuna" VALUES (310, 'Maullín', 13)
INTO "Comuna" VALUES (311, 'Osorno', 13)
INTO "Comuna" VALUES (312, 'Palena', 13)
INTO "Comuna" VALUES (313, 'Puerto Montt', 13)
INTO "Comuna" VALUES (314, 'Puerto Octay', 13)
INTO "Comuna" VALUES (315, 'Puerto Varas', 13)
INTO "Comuna" VALUES (316, 'Puqueldón', 13)
INTO "Comuna" VALUES (317, 'Purranque', 13)
INTO "Comuna" VALUES (318, 'Puyehue', 13)
INTO "Comuna" VALUES (319, 'Queilén', 13)
INTO "Comuna" VALUES (320, 'Quellón', 13)
INTO "Comuna" VALUES (321, 'Quemchi', 13)
INTO "Comuna" VALUES (322, 'Quinchao', 13)
INTO "Comuna" VALUES (323, 'Río Negro', 13)
INTO "Comuna" VALUES (324, 'San Juan de la Costa', 13)
INTO "Comuna" VALUES (325, 'San Pablo', 13)
INTO "Comuna" VALUES (326, 'Aysén', 14)
INTO "Comuna" VALUES (327, 'Chile Chico', 14)
INTO "Comuna" VALUES (328, 'Cisnes', 14)
INTO "Comuna" VALUES (329, 'Cochrane', 14)
INTO "Comuna" VALUES (330, 'Coihaique', 14)
INTO "Comuna" VALUES (331, 'Guaitecas', 14)
INTO "Comuna" VALUES (332, 'Lago Verde', 14)
INTO "Comuna" VALUES (333, 'O’Higgins', 14)
INTO "Comuna" VALUES (334, 'Río Ibáñez', 14)
INTO "Comuna" VALUES (335, 'Tortel', 14)
INTO "Comuna" VALUES (336, 'Antártica', 15)
INTO "Comuna" VALUES (337, 'Cabo de Hornos', 15)
INTO "Comuna" VALUES (338, 'Laguna Blanca', 15)
INTO "Comuna" VALUES (339, 'Natales', 15)
INTO "Comuna" VALUES (340, 'Porvenir', 15)
INTO "Comuna" VALUES (341, 'Primavera', 15)
INTO "Comuna" VALUES (342, 'Punta Arenas', 15)
INTO "Comuna" VALUES (343, 'Río Verde', 15)
INTO "Comuna" VALUES (344, 'San Gregorio', 15)
INTO "Comuna" VALUES (345, 'Timaukel', 15)
INTO "Comuna" VALUES (346, 'Torres del Paine', 15)
SELECT dual.DUMMY FROM dual;